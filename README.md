readnml - Quick and effective code for reading Fortran 90 namelists into Python dictionaries. Compatible with both Python 2.x and 3.x.

The only package imported is "re".

To use:
<code>
import readnml
myNamelist = readnml.ReadNML("myfile.nml")
</code>
Or, create sub-dictionaries for each namelist block:
<code>
myNamelist = readnml.ReadNML("myfile.nml",nest=True)
</code>
